import React , {Component} from 'react';
import { Button, Container, Table, TableBody, TableCell, TableHead, TableRow, TextField } from '@material-ui/core';
import axios from 'axios';

class App extends Component{
  constructor(props){
    super(props);
    this.state ={
      cityName:'lahore',
      sunrise:'',
      sunset:'',
      speed:'',
      description:'',
      main:'',
      humidity:'',
      pressure:'',
      icon:'',
      mainStatus:'',
      apiCityName:'',

    };

  }

  componentDidMount(){
    this.getWeatherData();
  }

getWeatherData =async ()=>{

  let response = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${this.state.cityName}&appid=bf7de6fda469dd0458b81316bbd61f5a`)
  if(response.data){
    console.log(response.data);
     this.setState({
    sunrise:response.data.sys.sunrise,
    sunset:response.data.sys.sunset,
    humidity:response.data.main.humidity,
    pressure:response.data.main.pressure,
    temp: response.data.main.temp,
    speed:response.data.wind.speed,
    icon:response.data.weather[0].icon,
    mainStatus: response.data.weather[0].main,
    apiCityName:response.data.name

  })
  }

}
  
onChangeHandle(event){
  this.setState({
    [event.target.name] : event.target.value
  })

}

getWeatherUpdates =() =>{
  this.getWeatherData();
}


  render(){
    const iconApi = 'https://openweathermap.org/img/w/' + this.state.icon + '.png';
    return(
      <div>
         <Container maxWidth="sm">
      <h1>Weather app</h1>
        <TextField id="outlined-basic" name="cityName" label="City Name" variant="outlined" onChange={(e)=>this.onChangeHandle(e)} />
        <br />
        <br />
      <Button variant="contained" color="secondary" onClick={()=>this.getWeatherUpdates()}>
        search
      </Button>
<br/><br/>
  <img id="wicon" src={iconApi} alt="Weather icon" />
      <h2>{this.state.apiCityName} |  Temp: F {this.state.temp} </h2>
      <Table  aria-label="simple table">
   
   
        <TableHead>
          <TableRow>
          
 
            <TableCell align="left">sunrise</TableCell>
            <TableCell align="left">sunset</TableCell>
            <TableCell align="left">speed</TableCell>
            <TableCell align="left">humidity</TableCell>
            <TableCell align="left">pressure</TableCell>
         
          </TableRow>
        </TableHead>
        <TableBody>
            <TableRow>
     
            <TableCell align="left">{this.state.sunrise}</TableCell>
            <TableCell align="left">{this.state.sunset}</TableCell>
    <TableCell align="left">{this.state.speed}</TableCell>
    <TableCell align="left">{this.state.humidity}</TableCell>
            
            <TableCell align="left">{this.state.pressure}</TableCell>
            </TableRow>
        </TableBody>
      </Table>

    </Container>
      </div>
  );
    
  }
}


export default App;
